from datetime import datetime

# datetime object containing current date and time
now = datetime.now()

class Dummy:
    def __init__(self, bias, baseline=float(1/3)):
      self.__bias = bias   # private
      self.__baseline = baseline   # private

    # read-only property for bias
    @property
    def bias(self):
        return self.__bias

    # read-only property for baseline
    @property
    def baseline(self):
        return self.__baseline

    # function to calculate sum of baseline and bias multiplier times
    def mul(self,multiplier):
        for i in range(multiplier):
            self.__baseline = self.__baseline + self.__bias

        return self.__baseline

    # function returns current date and time in given format
    def dt(self):
        dt_string = now.strftime("%H:%M:%S %d/%m/%Y")
        return dt_string
    
# print result value of baseline rounded by 3
print(round(Dummy(2).mul(2),3))

# print current date and time in given format
print(Dummy(2).dt())
